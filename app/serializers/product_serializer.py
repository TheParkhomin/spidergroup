from rest_framework import serializers

from app.models.product import Product

from .category_serializer import CategorySerializer
from .organization_product_serializer import OrganizationProductSerializer
from .organization_serializer import OrganizationSerializer


class ProductSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False)
    prices = OrganizationProductSerializer(many=True)
    organizations = OrganizationSerializer(many=True)

    class Meta:
        model = Product
        fields = ("id", "name", "category", "prices", "organizations")
