from rest_framework import serializers

from app.serializers.organization_network_serializer import OrganizationNetworkSerializer
from app.serializers.district_serializer import DistrictSerializer
from app.serializers.category_serializer import CategorySerializer

from app.models.organization import Organization
from app.models.product import Product
from app.models.organization_product import OrganizationProduct


class ProductForOrganizationSerializer(serializers.ModelSerializer):
    category = CategorySerializer(many=False)

    class Meta:
        model = Product
        fields = ("name", "category")


class OrganizationProductForOrganizationSerializer(serializers.ModelSerializer):
    product = ProductForOrganizationSerializer(many=False)

    class Meta:
        model = OrganizationProduct
        fields = ("product", "price")


class OrganizationSerializer(serializers.ModelSerializer):
    network = OrganizationNetworkSerializer(many=False)
    districts = DistrictSerializer(many=True)

    class Meta:
        model = Organization
        fields = ("id", "name", "network", "description", "districts")


class OrganizationDetailSerializer(serializers.ModelSerializer):
    network = OrganizationNetworkSerializer(many=False)
    districts = DistrictSerializer(many=True)
    products = OrganizationProductForOrganizationSerializer(many=True)

    class Meta:
        model = Organization
        fields = ("id", "name", "network", "description", "districts", "products")
