from rest_framework import serializers

from app.models.organization_network import OrganizationNetwork


class OrganizationNetworkSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrganizationNetwork
        fields = ("id", "name")
