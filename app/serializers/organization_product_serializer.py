from rest_framework import serializers

from app.models.organization_product import OrganizationProduct
from app.serializers.organization_serializer import OrganizationSerializer


class OrganizationProductSerializer(serializers.ModelSerializer):
    organization = OrganizationSerializer(many=False)

    class Meta:
        model = OrganizationProduct
        fields = ("organization", "price")
