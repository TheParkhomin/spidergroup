from rest_framework import serializers

from app.models.district import District


class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = District
        fields = ("id", "name")
