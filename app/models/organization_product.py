from django.db import models

from .product import Product


class OrganizationProduct(models.Model):
    organization = models.ForeignKey("Organization", on_delete=models.PROTECT, verbose_name="Предприятие")
    product = models.ForeignKey(Product, on_delete=models.PROTECT, verbose_name="Товар")
    price = models.IntegerField(verbose_name="Цена")

    class Meta:
        verbose_name = "Предприятие - Товар"
        verbose_name_plural = "Предприятия - Товары"

    def __str__(self):
        return "{} - {} - {}".format(self.organization, self.product, self.price)
