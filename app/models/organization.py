from django.db import models


class Organization(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название")
    network = models.ForeignKey("OrganizationNetwork", on_delete=models.PROTECT, verbose_name="Сеть предприятий")
    description = models.TextField(verbose_name="Описание")
    districts = models.ManyToManyField("District", verbose_name="Районы")
    product_organizations = models.ManyToManyField("Product", through="OrganizationProduct", verbose_name="Продукты")

    class Meta:
        verbose_name = "Предприятие"
        verbose_name_plural = "Предприятия"

    def __str__(self):
        return self.name

    @property
    def products(self):
        return list(self.organizationproduct_set.all())
