from django.db import models


class OrganizationNetwork(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название")

    class Meta:
        verbose_name = "Сеть предприятий"
        verbose_name_plural = "Сети предприятий"

    def __str__(self):
        return self.name
