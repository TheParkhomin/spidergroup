from django.db import models

from .organization import Organization


class Product(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название")
    category = models.ForeignKey("Category", on_delete=models.PROTECT, verbose_name="Категория")

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товары"

    def __str__(self):
        return "{} - {}".format(self.name, self.category)

    @property
    def organizations(self):
        organization_ids = self.organizationproduct_set.values_list("organization_id", flat=True)
        return list(Organization.objects.filter(id__in=organization_ids))

    @property
    def prices(self):
        return list(self.organizationproduct_set.all())
