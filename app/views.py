from rest_framework.decorators import api_view
from rest_framework.response import Response

from .filters.organization_filter import OrganizationFilter

from .decorators import check_exist_object

from .models.organization import Organization
from .models.product import Product
from .models.district import District

from .serializers.organization_serializer import OrganizationDetailSerializer
from .serializers.product_serializer import ProductSerializer


@api_view(["GET"])
@check_exist_object("Organization")
def get_detail_organization(request, pk):
    organization = Organization.objects.get(pk=pk)
    organization_serializer = OrganizationDetailSerializer(organization)
    return Response(data=organization_serializer.data, status=200)


@api_view(["GET"])
def get_organization_by_district(request, district_id):
    try:
        district = District.objects.get(id=district_id)
    except District.DoesNotExist:
        return Response(data="District not found", status=404)
    except ValueError:
        return Response(data="Invalid value for primary key of District", status=400)

    organizations = Organization.objects.filter(districts=district)

    organization_filter = OrganizationFilter(request.GET, queryset=organizations)
    organization_serializer = OrganizationDetailSerializer(organization_filter.qs.distinct(), many=True)
    return Response(data=organization_serializer.data, status=200)


@api_view(["GET"])
@check_exist_object("Product")
def get_detail_product(request, pk):
    product = Product.objects.get(pk=pk)
    product_serializer = ProductSerializer(product)
    return Response(data=product_serializer.data, status=200)
