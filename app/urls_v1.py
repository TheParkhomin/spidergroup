from django.urls import path

from .views import get_detail_product, get_detail_organization, get_organization_by_district

urlpatterns = [
    path(r'products/detail/<pk>/', get_detail_product),

    path(r'organizations/detail/<pk>/', get_detail_organization),
    path(r'organizations/<district_id>/', get_organization_by_district)
]
