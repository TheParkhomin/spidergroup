from functools import wraps

from rest_framework.response import Response

from django.apps import apps


def check_exist_object(model_name):
    def inner_function(func):
        @wraps(func)
        def decorator(request, **kwargs):
            pk = kwargs.get("pk")
            model = apps.get_model(app_label="app", model_name=model_name)
            try:
                model.objects.get(pk=pk)
            except model.DoesNotExist:
                return Response(data="{} not found".format(model_name), status=404)
            except ValueError:
                return Response(data="Invalid value for primary key of {}".format(model_name), status=400)
            return func(request, **kwargs)

        return decorator

    return inner_function
