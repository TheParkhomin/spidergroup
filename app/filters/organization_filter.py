import django_filters


from app.models.organization import Organization


class OrganizationFilter(django_filters.FilterSet):
    min_price = django_filters.NumberFilter(field_name="organizationproduct__price", lookup_expr="gte")
    max_price = django_filters.NumberFilter(field_name="organizationproduct__price", lookup_expr="lte")
    product__category = django_filters.CharFilter(field_name="product_organizations__category__name")
    product__name = django_filters.CharFilter(field_name="product_organizations__name")

    class Meta:
        model = Organization
        fields = ("name", "product__name", "product__category", "max_price", "min_price")
