from django.contrib import admin

from .models.district import District
from .models.product import Product
from .models.category import Category
from .models.organization import Organization
from .models.organization_network import OrganizationNetwork
from .models.organization_product import OrganizationProduct


admin.site.register(District)
admin.site.register(Product)
admin.site.register(Category)
admin.site.register(OrganizationNetwork)


class OrganizationProductInline(admin.TabularInline):
    model = OrganizationProduct
    extra = 1


class OrganizationAdmin(admin.ModelAdmin):
    inlines = [OrganizationProductInline]


admin.site.register(Organization, OrganizationAdmin)
