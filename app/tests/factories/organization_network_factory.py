import factory

from app.models.organization_network import OrganizationNetwork


class OrganizationNetworkFactory(factory.DjangoModelFactory):
    class Meta:
        model = OrganizationNetwork
