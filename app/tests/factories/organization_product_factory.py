import factory

from app.models.organization_product import OrganizationProduct


class OrganizationProductFactory(factory.DjangoModelFactory):
    class Meta:
        model = OrganizationProduct
