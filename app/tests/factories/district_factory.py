import factory

from app.models.district import District


class DistrictFactory(factory.DjangoModelFactory):
    class Meta:
        model = District
