import factory

from app.models.category import Category


class CategoryFactory(factory.DjangoModelFactory):
    class Meta:
        model = Category
