import factory

from app.models.product import Product


class ProductFactory(factory.DjangoModelFactory):
    class Meta:
        model = Product
