import factory

from app.models.organization import Organization


class OrganizationFactory(factory.DjangoModelFactory):
    class Meta:
        model = Organization
