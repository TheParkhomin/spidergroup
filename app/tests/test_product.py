from django.test import TestCase

from .factories.organization_factory import OrganizationFactory
from .factories.organization_product_factory import OrganizationProductFactory
from .factories.organization_network_factory import OrganizationNetworkFactory
from .factories.district_factory import DistrictFactory
from .factories.product_factory import ProductFactory
from .factories.category_factory import CategoryFactory

from app.views import get_detail_product


class ProductTest(TestCase):
    def setUp(self):
        self.category_1 = CategoryFactory(name="category_1")

        self.product_1 = ProductFactory(name="product_1", category=self.category_1)
        self.organization_network_1 = OrganizationNetworkFactory(name="organization_network_1")
        self.organization = OrganizationFactory(
            name="Organization_1", network=self.organization_network_1, description="description_of_organization_1",
        )
        self.district_1 = DistrictFactory(name="district1")
        self.district_2 = DistrictFactory(name="district2")
        self.organization.districts.set([self.district_1, self.district_2])
        self.organization.product_organizations.set(
            [OrganizationProductFactory(organization=self.organization, product=self.product_1, price=3000).product]
        )

        self.category_2 = CategoryFactory(name="category_2")

        self.product_2 = ProductFactory(name="product_2", category=self.category_2)

        self.product_3 = ProductFactory(name="product_3", category=self.category_2)

        self.organization_network_2 = OrganizationNetworkFactory(name="organization_network_2")
        self.organization_2 = OrganizationFactory(
            name="Organization_2", network=self.organization_network_2, description="description_of_organization_2",
        )
        self.district_3 = DistrictFactory(name="district3")
        self.district_4 = DistrictFactory(name="district4")
        self.organization_2.districts.set([self.district_1, self.district_3, self.district_4])
        self.organization_2.product_organizations.set(
            [
                OrganizationProductFactory(
                    organization=self.organization_2, product=self.product_1, price=3300
                ).product,
                OrganizationProductFactory(
                    organization=self.organization_2, product=self.product_3, price=4000
                ).product,
            ]
        )

    def test_product_detail_not_found(self):
        url = "/api/v1/products/detail/{}/".format(22)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.resolver_match.func, get_detail_product)

    def test_product_detail_invalid_query(self):
        url = "/api/v1/products/detail/{}/".format('1d')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.resolver_match.func, get_detail_product)

    def test_product_detail(self):
        url = "/api/v1/products/detail/{}/".format(self.product_1.id)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, get_detail_product)

        data = response.data

        self.assertEqual(data.get("id"), self.product_1.id)
        self.assertEqual(data.get("name"), self.product_1.name)

        category = data.get("category")
        self.assertNotEqual(category, None)

        self.assertEqual(category.get("id"), self.category_1.id)
        self.assertEqual(category.get("name"), self.category_1.name)

        prices = data.get("prices")
        self.assertNotEqual(prices, None)
        price_1 = prices[0]
        self.assertEqual(price_1.get("price"), self.organization.organizationproduct_set.first().price)
        price_organization = price_1.get("organization")
        self.assertNotEqual(price_organization, None)

        self.assertEqual(price_organization.get("id"), self.organization.id)
        self.assertEqual(price_organization.get("name"), self.organization.name)

        price_organization_network = price_organization.get("network")

        self.assertNotEqual(price_organization_network, None)
        self.assertEqual(price_organization_network.get("id"), self.organization_network_1.id)
        self.assertEqual(price_organization_network.get("name"), self.organization_network_1.name)

        price_organization_disctricts = price_organization.get("districts")
        self.assertNotEqual(price_organization_disctricts, None)
        self.assertEqual(len(price_organization_disctricts), 2)

        price_organization_district_1 = price_organization_disctricts[0]
        self.assertEqual(price_organization_district_1.get("id"), self.district_1.id)
        self.assertEqual(price_organization_district_1.get("name"), self.district_1.name)

        organizations = data.get("organizations")
        self.assertNotEqual(organizations, None)
