from django.test import TestCase

from .factories.organization_factory import OrganizationFactory
from .factories.organization_product_factory import OrganizationProductFactory
from .factories.organization_network_factory import OrganizationNetworkFactory
from .factories.district_factory import DistrictFactory
from .factories.product_factory import ProductFactory
from .factories.category_factory import CategoryFactory

from app.views import get_detail_organization, get_organization_by_district


class OrganizationTest(TestCase):
    def setUp(self):
        self.category_1 = CategoryFactory(name="category_1")

        self.product_1 = ProductFactory(name="product_1", category=self.category_1)
        self.organization_network_1 = OrganizationNetworkFactory(name="organization_network_1")
        self.organization = OrganizationFactory(
            name="Organization_1", network=self.organization_network_1, description="description_of_organization_1",
        )
        self.district_1 = DistrictFactory(name="district1")
        self.district_2 = DistrictFactory(name="district2")
        self.organization.districts.set([self.district_1, self.district_2])
        self.organization.product_organizations.set(
            [OrganizationProductFactory(organization=self.organization, product=self.product_1, price=3000).product]
        )

        self.category_2 = CategoryFactory(name="category_2")

        self.product_2 = ProductFactory(name="product_2", category=self.category_2)

        self.product_3 = ProductFactory(name="product_3", category=self.category_2)

        self.organization_network_2 = OrganizationNetworkFactory(name="organization_network_2")
        self.organization_2 = OrganizationFactory(
            name="Organization_2", network=self.organization_network_2, description="description_of_organization_2",
        )
        self.district_3 = DistrictFactory(name="district3")
        self.district_4 = DistrictFactory(name="district4")
        self.organization_2.districts.set([self.district_1, self.district_3, self.district_4])
        self.organization_2.product_organizations.set(
            [
                OrganizationProductFactory(
                    organization=self.organization_2, product=self.product_1, price=3300
                ).product,
                OrganizationProductFactory(
                    organization=self.organization_2, product=self.product_3, price=4000
                ).product,
            ]
        )

    def test_organization_detail_not_found(self):
        url = "/api/v1/organizations/detail/{}/".format(22)
        response = self.client.get(url)

        self.assertEqual(response.resolver_match.func, get_detail_organization)
        self.assertEqual(response.status_code, 404)

    def test_organization_detail_invalid_query(self):
        url = "/api/v1/organizations/detail/{}/".format('324ffs')
        response = self.client.get(url)

        self.assertEqual(response.resolver_match.func, get_detail_organization)
        self.assertEqual(response.status_code, 400)

    def test_organization_detail(self):
        url = "/api/v1/organizations/detail/{}/".format(self.organization.id)
        response = self.client.get(url)

        self.assertEqual(response.resolver_match.func, get_detail_organization)
        self.assertEqual(response.status_code, 200)
        data = response.data

        organization_id = data.get("id")
        self.assertEqual(organization_id, self.organization.id)

        organization_name = data.get("name")
        self.assertEqual(organization_name, self.organization.name)

        network = data.get("network")
        self.assertNotEqual(network, None)

        self.assertEqual(network.get("id"), self.organization_network_1.id)
        self.assertEqual(network.get("name"), self.organization_network_1.name)

        description = data.get("description")
        self.assertEqual(description, self.organization.description)

        districts = data.get("districts")
        self.assertNotEqual(districts, None)
        self.assertEqual(len(districts), 2)

        self.assertEqual(districts[0].get("id"), self.district_1.id)
        self.assertEqual(districts[0].get("name"), self.district_1.name)
        self.assertEqual(districts[1].get("id"), self.district_2.id)
        self.assertEqual(districts[1].get("name"), self.district_2.name)

        products = data.get("products")
        self.assertNotEqual(products, None)

        self.assertEqual(len(products), 1)
        product_organization = products[0]

        product = product_organization.get("product")
        self.assertNotEqual(product, None)
        product_name = product.get("name")
        self.assertEqual(product_name, self.product_1.name)

        product_category = product.get("category")
        self.assertNotEqual(product_category, None)
        product_category_name = product_category.get("name")
        self.assertEqual(product_category_name, self.category_1.name)
        product_price = product_organization.get("price")

        self.assertEqual(product_price, self.organization.organizationproduct_set.first().price)

    def test_organizations_by_district_not_found(self):
        url = "/api/v1/organizations/{}/".format(22)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.resolver_match.func, get_organization_by_district)

    def test_organizations_by_district(self):
        url = "/api/v1/organizations/{}/".format(self.district_1.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, get_organization_by_district)
        data = response.data

        self.assertEqual(len(data), 2)

        organization_1 = data[0]
        self.assertEqual(organization_1.get("id"), self.organization.id)

        organization_2 = data[1]
        self.assertEqual(organization_2.get("id"), self.organization_2.id)

    def test_organizations_by_district_with_search_by_name(self):
        url = "/api/v1/organizations/{}/?name=Organization_1".format(self.district_1.id)
        response = self.client.get(url)
        data = response.data

        self.assertEqual(len(data), 1)

        organization_1 = data[0]
        self.assertEqual(organization_1.get("id"), self.organization.id)

        url = "/api/v1/organizations/{}/?name=Organization_12".format(self.district_1.id)
        response = self.client.get(url)
        data = response.data

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, get_organization_by_district)

        self.assertEqual(len(data), 0)

    def test_organizations_by_district_with_search_by_product_name(self):
        url = "/api/v1/organizations/{}/?product__name=product_3".format(self.district_1.id)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, get_organization_by_district)

        data = response.data
        self.assertEqual(len(data), 1)

        organization_2 = data[0]
        self.assertEqual(organization_2.get("id"), self.organization_2.id)

        url = "/api/v1/organizations/{}/?product__name=product_333".format(self.district_1.id)
        response = self.client.get(url)

        data = response.data
        self.assertEqual(len(data), 0)

    def test_organizations_by_district_with_search_by_product_category(self):
        url = "/api/v1/organizations/{}/?product__category=category_1".format(self.district_1.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, get_organization_by_district)

        data = response.data
        self.assertEqual(len(data), 2)

        organization_1 = data[0]
        self.assertEqual(organization_1.get("id"), self.organization.id)

        organization_2 = data[1]
        self.assertEqual(organization_2.get("id"), self.organization_2.id)

        url = "/api/v1/organizations/{}/?product__category=category_1ggfd".format(self.district_1.id)
        response = self.client.get(url)

        data = response.data
        self.assertEqual(len(data), 0)

    def test_organizations_by_district_with_min_price_filter(self):
        url = "/api/v1/organizations/{}/?min_price=3500".format(self.district_1.id)
        response = self.client.get(url)

        data = response.data
        self.assertEqual(len(data), 1)

        organization_2 = data[0]
        self.assertEqual(organization_2.get("id"), self.organization_2.id)

    def test_organizations_by_district_with_max_price_filter(self):
        url = "/api/v1/organizations/{}/?max_price=3900".format(self.district_1.id)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, get_organization_by_district)

        data = response.data
        self.assertEqual(len(data), 2)

        organization_1 = data[0]
        self.assertEqual(organization_1.get("id"), self.organization.id)

        organization_2 = data[1]
        self.assertEqual(organization_2.get("id"), self.organization_2.id)

    def test_organizations_by_district_with_product_category_and_min_price_filter(self):
        url = "/api/v1/organizations/{}/?product__category=category_1&min_price=3700".format(self.district_1.id)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.resolver_match.func, get_organization_by_district)

        data = response.data
        self.assertEqual(len(data), 1)

        organization = data[0]
        self.assertEqual(organization.get("id"), self.organization_2.id)
